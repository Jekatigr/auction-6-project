#include "Group.h"

void Group::add(Client* ptr) 
{
	objects[ptr->getEmail()] = ptr;
}

Group::iterator Group::begin() 
{
	return objects.begin();
}

Group::iterator Group::end() 
{
	return objects.end();
}

Client* Group::operator[](const string& email) 
{
	return objects.at(email);
}