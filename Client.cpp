#include"Client.h"
#include <iostream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

Client::Client(void) : fname(string()), lname(string()), email(string()), passwd(string()) {}

Client::Client(Client const &c) : fname(c.getFname()), lname(c.getLname()), email(c.getEmail()), passwd(c.getPasswd()) {}

Client::Client (string &fname, string &lname, string &email, string &passwd) :
fname(fname), lname(lname), email(email), passwd(passwd) {}

 void Client::setFname(const string& a){
	 fname = a;
}

 void Client::setLname(const string& a){
	 lname = a;
}

  void Client::setEmail(const string& a){
	 email = a;
}

  void Client::setPasswd(const string& a){
	 passwd = a;
}

 string Client::getFname() const{
	 return fname ;
}

 string Client::getLname() const{
	 return lname ;
}

  string Client::getEmail() const{
	 return email ;
}

  string Client::getPasswd() const{
	 return passwd;
}

  bool Client::verifyPasswd(string passwd)
  {
	  if (passwd.compare(Client::passwd) == 0)
	  {
		  return true;
	  }
	  return false;
  }

  istream &operator>> (istream &stream, Client& c) {
	  string s;
	  getline(stream, s);
	  c.setFname(s);
	  getline(stream, s);
	  c.setLname(s);
	  getline(stream, s);
	  c.setEmail(s);
	  getline(stream, s);
	  c.setPasswd(s);
	  return stream;
  }

vector<int>::iterator Client::beginBids()
{
	return this->bids.begin();
}

vector<int>::iterator Client::endBids()
{
	return this->bids.end();
}

vector<int>::iterator Client::beginOfferings()
{
	return this->offerings.begin();
}

vector<int>::iterator Client::endOfferings()
{
	return this->offerings.end();
}

void Client::addBid(int item)
{
	this->bids.push_back(item);
}

void Client::addOffering(int item)
{
	this->offerings.push_back(item);
}
