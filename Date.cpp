#include "date.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <sstream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

using namespace std;



// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

Date::Date (int month, int day, int year, int hour, int minute, int second) : month(month), day(day),
		year(year), hour(hour), minute(minute), second(second)
	{
	}

Date::Date (void) : month(0), day(0),
		year(0), hour(0), minute(0), second(0)
	{
	}

void Date::setMonth(int &month)
{
	if (month > 12) 
	{
		throw "Month in data is not correct.";	
	}
	this->month = month;
}

void Date::setDay(int &day)
{
	if (day > 31) 
	{
		throw "Day in data is not correct.";	
	}
	this->day = day;
}

void Date::setYear(int &year)
{
	this->year = year;
}

void Date::setHour(int &hour)
{
	if (hour > 24) 
	{
		throw "Hour in data is not correct.";	
	}
	this->hour = hour;
}

void Date::setMinute(int &minute)
{
	if (minute > 60) 
	{
		throw "Minute in data is not correct.";	
	}
	this->minute = minute;
}

void Date::setSecond(int &second)
{
	if (second > 60) 
	{
		throw "Second in data is not correct.";	
	}
	this->second = second;
}

int Date::getMonth(void) const
{
	return this->month;
}

int Date::getDay(void) const
{
	return this->day;
}


int Date::getYear(void) const
{
	return this->year;
}

int Date::getHour(void) const
{
	return this->hour;
}

int Date::getMinute(void) const
{
	return this->minute;
}

int Date::getSecond(void) const
{
	return this->second;
}

ostream &operator<< (ostream &stream, const Date& date)
{
	char filler = '0';
	char separator_first = ' ';
	char separator_second = '/';
	char separator_third = ':';

	stream << setfill(filler) << setw(2) << date.getMonth() << separator_second;
	stream << setfill(filler) << setw(2) << date.getDay() << separator_second;
	stream << setfill(filler) << setw(4) << date.getYear() << separator_first;

	stream << setfill(filler) << setw(2) << date.getHour() << separator_third;
	stream << setfill(filler) << setw(2) << date.getMinute() << separator_third;
	stream << setfill(filler) << setw(2) << date.getSecond();
	return stream;
}

istream &operator>>(istream& stream, Date& date)
{
	string separator_first = " ";
	string separator_second = "/";
	string separator_third = ":";
	string main;
	getline(stream, main);
	main = trim(main);
	string firstPart = main.substr(0, main.find(separator_first));
	string secondPart = main.substr(main.find(separator_first) + 1, main.size());

	int position = firstPart.find(separator_second);
	int month = stoi(firstPart.substr(0, position));
	firstPart = firstPart.substr(position + 1, firstPart.length());
	position = firstPart.find(separator_second);
	int day = stoi(firstPart.substr(0, position));
	firstPart = firstPart.substr(position + 1, firstPart.length());
	int year = stoi(firstPart);

	position = secondPart.find(separator_third);
	int hour = stoi(secondPart.substr(0, position));
	secondPart = secondPart.substr(position + 1, secondPart.length());
	position = secondPart.find(separator_third);
	int minute = stoi(secondPart.substr(0, position));
	secondPart = secondPart.substr(position + 1, secondPart.length());
	int sec = stoi(secondPart);

	date.setDay(day);
	date.setMonth(month);
	date.setYear(year);
	date.setHour(hour);
	date.setMinute(minute);
	date.setSecond(sec);
	return stream;
}

bool Date::operator==(const Date &rhs)
{
	if (getYear() != rhs.getYear())
	{
		return false;
	}

	if (getMonth() != rhs.getMonth())
	{
		return false;
	}

	if (getDay() != rhs.getDay())
	{
		return false;
	}

	if (getHour() != rhs.getHour())
	{
		return false;
	}

	if (getMinute() != rhs.getMinute())
	{
		return false;
	}

	if (getSecond() != rhs.getSecond())
	{
		return false;
	}
	
	return true;
}

bool Date::operator<(const Date &rhs)
{
	if (getYear() < rhs.getYear())
	{
		return true;
	}

	if (getYear() > rhs.getYear())
	{
		return false;
	}


	if (getMonth() < rhs.getMonth())
	{
		return true;
	}

	if (getMonth() > rhs.getMonth())
	{
		return false;
	}


	if (getDay() < rhs.getDay())
	{
		return true;
	}

	if (getDay() > rhs.getDay())
	{
		return false;
	}

	if (getHour() < rhs.getHour())
	{
		return true;
	}

	if (getHour() > rhs.getHour())
	{
		return false;
	}

	if (getMinute() < rhs.getMinute())
	{
		return true;
	}

	if (getMinute() > rhs.getMinute())
	{
		return false;
	}

	if (getSecond() < rhs.getSecond())
	{
		return true;
	}

	return false;
}
