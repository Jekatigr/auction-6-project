#include "Categories.h"

const int Categories::NO_PARENT = 0;
const int Categories::TOP_LEVEL = 0;

Categories::iterator Categories::begin()
{
	return objects.begin();
}

Categories::iterator Categories::end()
{
	return objects.end();
}

void Categories::add(Category* ptr)
{
	objects[ptr->getNumber()] = ptr;
}

Category* Categories::operator[](const int& number)
{
	return objects.at(number);
}