#include "Advertisement.h"

using namespace std;

Advertisement::Advertisement() : 
number(0), quantity(0), title(""), seller_email(""), body(""), start(Date()), close(Date()) {}

Advertisement::Advertisement(string title, string seller_email, string body, Date start, Date close, int quantity) : 
number(0), title(title), seller_email(seller_email), body(body), start(start), close(close), quantity(quantity) {}

Advertisement::Advertisement(const Advertisement &a) : 
number(a.number), quantity(a.quantity), title(a.title), seller_email(a.seller_email), body(a.body), start(a.start), close(a.close) {}

static string trim(string &s) 
{
	s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
	s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
	return s;
}

static Date parseDate(string &main) 
{
	string separator_first = " ";
	string separator_second = "/";
	string separator_third = ":";

	string firstPart = main.substr(0, main.find(separator_first));
	string secondPart = main.substr(main.find(separator_first) + 1, main.size());

	int position = firstPart.find(separator_second);
	int month = stoi(firstPart.substr(0, position));
	firstPart = firstPart.substr(position + 1, firstPart.length());
	position = firstPart.find(separator_second);
	int day = stoi(firstPart.substr(0, position));
	firstPart = firstPart.substr(position + 1, firstPart.length());
	int year = stoi(firstPart);

	position = secondPart.find(separator_third);
	int hour = stoi(secondPart.substr(0, position));
	secondPart = secondPart.substr(position + 1, secondPart.length());
	position = secondPart.find(separator_third);
	int minute = stoi(secondPart.substr(0, position));
	secondPart = secondPart.substr(position + 1, secondPart.length());
	int sec = stoi(secondPart);
	Date date(month, day, year, hour, minute, sec);
	
	return date;
}

void Advertisement::setStart(const Date &start) 
{
	this->start = start;
}

Date Advertisement::getStart() const
{
	return this->start;
}


void Advertisement::setClose(const Date &close)
{
	this->close = close;
}

Date Advertisement::getClose() const 
{
	return this->close;
}


void Advertisement::setTitle(string title) 
{
	this->title = title;
}

string Advertisement::getTitle() const
{
	return this->title;
}


void Advertisement::setBody(string body)
{
	this->body = body;
}

string Advertisement::getBody() const
{
	return this->body;
}


void Advertisement::setNumber(int number)
{
	this->number = number;
}

int Advertisement::getNumber() const
{
	return this->number;
}


void Advertisement::setEmail(string email)
{
	this->seller_email = email;
}

string Advertisement::getEmail() const
{
	return this->seller_email;
}


void Advertisement::setQuantity(int quantity)
{
	this->quantity = quantity;
}

int Advertisement::getQuantity() const
{
	return this->quantity;
}


bool Advertisement::operator==(const Advertisement &a) const
{
	if (this->number == a.getNumber())
	{
		return true;
	}
	return false;
}

istream &operator>>(istream &stream, Advertisement &a)
{
	Date date;
	string tempStr;

	getline(stream, tempStr);
	trim(tempStr);
	a.setTitle(tempStr);  

	getline(stream, tempStr);
	trim(tempStr);
	a.setEmail(tempStr);

	getline(stream, tempStr);
	trim(tempStr);
	a.setQuantity(stoi(tempStr));

	getline(stream, tempStr);
	trim(tempStr); 
	a.setStart(parseDate(tempStr));      

	getline(stream, tempStr);
	trim(tempStr);
	a.setClose(parseDate(tempStr));  

	getline(stream, tempStr);
	trim(tempStr);
	a.setBody(tempStr);

	return stream;
}

priority_queue<Bid>& Advertisement::getBids(void)
{
	return this->bids;
}
vector<Bid> Advertisement::getTopDutchBids(void) const {
	vector<Bid> result;
	priority_queue<Bid> tempBids = bids;

	if (tempBids.empty()) {
		return result;
	}
	else {
		for (int i = 0; i < this->getQuantity();) {
			Bid b = tempBids.top();
			result.push_back(b);
			tempBids.pop();
			i += b.getQuantity();
		}
	}
	return result;
}