#include"bidhistory.h"

void displayBidHistory(ostringstream &oss, Advertisement* ad)
{
	Client* client = NULL;
	client = users[ad->getEmail()];
	int bid_number = 0;
	vector<Bid> bids = ad->getTopDutchBids();
	string tagB = "<B>";
	string tagBe = "</B>";
	string tagbr = "<br>";
	oss << "<table border=0 width=100%><tr><td align=center width=20%>" << endl;
	oss << "<tr>"<< endl;
	oss << tagB << "AD Title is: " << tagBe << ad->getTitle() << tagbr << endl;
	oss << tagB << "Last name is: " << tagBe << client->getLname() << tagbr << endl;
	oss << tagB << "First name is: " << tagBe << client->getFname() << tagbr << endl;
	oss << tagB << "It was posted: " << tagBe << ad->getStart() << tagbr << endl;
	oss << tagB << "It closes: " << tagBe << ad->getClose() << tagbr << endl;
	oss << tagB << "Quantity is: " << tagBe << ad->getQuantity() << tagbr << endl;
	oss << tagB << "Number of bids is: " << tagBe << ad->getBids().size() << tagbr << tagbr << endl;

	oss << tagB << "The highest bid list: " << tagBe << tagbr << endl;
	for (vector<Bid>::iterator it = bids.begin(); it != bids.end(); it++)
	{
		oss << tagB << "�" << ++bid_number << tagBe << tagbr << endl;
		oss << tagB << "Email of Bidder is: " << it->getEmail() << tagBe << tagbr << endl;
		oss << tagB << "The amount of dollars of the bid is: " << it->getAmount() << tagBe << tagbr << endl;
		oss << tagB << "The number of items bid on: " << it->getQuantity() << tagBe << tagbr << endl;
		oss << tagB << "Auction Date: " << it->getDate() << tagBe << tagbr << tagbr << endl;
	}
	oss << tagbr<< "</tr></table>" << endl;
}