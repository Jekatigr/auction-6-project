#ifndef SORT_BY_PARAMETER_h;
#define SORT_BY_PARAMETER_h
#include "Advertisement.h"

class SortByParameter 
{
private:
    string criterion;

public:
    SortByParameter(string criterion);
    bool SortByParameter::operator()(Advertisement *a1, Advertisement *a2);
};
#endif