#include "Listing.h"
#include "SortByParameter.h"
using namespace std;

void Listing::add(Advertisement* ptr)
{
	objects.push_back(ptr);
}

Listing::iterator Listing::begin()
{
	return objects.begin();
}

Listing::iterator Listing::end()
{
	return objects.end();
}

Advertisement* Listing::operator[](const int& number)
{
	for (Listing::iterator i = begin(); i != end(); i++)
	{
		if ((*i)->getNumber() == number)
			return *i;
	}
	return NULL;
}

Listing Listing::sort(string field) 
{
    Listing new_obj(*this);
	//������������ ��� ��� ����, ����� ������ ��������������� � ������ ������
	//(���� ��� - ���������� ������� ��� ����)
    std::sort(new_obj.begin(), new_obj.end(), SortByParameter(field));
    return new_obj;
}

Listing Listing::filter(string keyword) 
{
	Listing result(*this);
	if(keyword.empty())
		return result;
	result.objects.erase(
					remove_if(result.begin(), result.end(), [&] (Advertisement* ad) -> bool
					{
						string title = ad->getTitle();
						string body = ad->getBody();
						std::transform(title.begin(), title.end(), title.begin(), ::tolower);
						std::transform(body.begin(), body.end(), body.begin(), ::tolower);
						std::transform(keyword.begin(), keyword.end(), keyword.begin(), ::tolower);
						return !(title.find(keyword) != string::npos || body.find(keyword) != string::npos);
					}),
					result.end()
				 );
	return result;
}