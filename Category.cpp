#include "Category.h"

Category::Category(void) : number(0), parent(0), name("") {}
Category::Category(int parent, string name) : number(0), parent(parent), name(name) {}

int Category::getNumber(void) const
{
	return number;
}

int Category::getParent(void) const
{
	return parent;
}

string Category::getName(void) const
{
	return name;
}

void Category::setNumber(int numb)
{
	this->number = numb;
}

void Category::setParent(int par)
{
	this->parent = par;
}
void Category::setName(string nm)
{
	this->name = nm;
}

void Category::addSubCategory(Category* cat) 
{
	sub_categories.push_back(cat);
}

void Category::addItem(int numb)
{
	items.push_back(numb);
}

void Category::findOfferings (Listing::iterator start, Listing::iterator finish, Listing &matches)
{
	for (Listing::iterator it = start; it != finish; it++) {   
		vector<int>::iterator search;   
		search = find(this->itemsBegin(), this->itemsEnd(), (*it)->getNumber());   
		if (search != this->itemsEnd()) 
		{   
			matches.add(*it);   
		}   
	}  
}

void Category::findOfferingsRecursive (Listing::iterator start, Listing::iterator finish, Listing &matches)
{
	findOfferings(start, finish, matches);   
	vector<Category *>::iterator it;   
	for (it = this->subCategoriesBegin(); it != this->subCategoriesEnd(); it++) 
	{   
		(*it)->findOfferingsRecursive(start, finish, matches);   
	}   
}

vector<int>::iterator Category::itemsBegin()
{
	return items.begin();
}

vector<int>::iterator Category::itemsEnd()
{
	return items.end();
}

vector<Category*>::iterator Category::subCategoriesBegin()
{
	return sub_categories.begin();
}

vector<Category*>::iterator Category::subCategoriesEnd()
{
	return sub_categories.end();
}

bool Category::operator==(const Category& rhs)
{
	return (this->getNumber() == rhs.getNumber());
}

istream &operator>>(istream &stream, Category &c)
{   
	int parent;
    string name;
    stream >> parent >> name;
    c.setParent(parent);
    c.setName(name);
    return stream;  
}   