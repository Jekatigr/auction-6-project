
#include "Bid.h"   

Bid::Bid(void) : email(""), amount(0), quantity(0){this->date = Date();}

Bid::Bid(const Bid &b) {
	email = b.getEmail();
	amount = b.getAmount();
	quantity = b.getQuantity();
	date = b.getDate();
}

Bid::Bid(string email, float amount, int quantity, Date date) :
email(email), amount(amount), quantity(quantity), date(date) {}

float Bid::getAmount() const{ return amount; }
Date Bid::getDate() const{ return date; }
int Bid::getQuantity() const{ return quantity; }
string Bid::getEmail() const{ return email; }

void Bid::setAmount(const float& f){ amount = f; }
void Bid::setDate(const Date& d){ date = d; }
void Bid::setEmail(const string& s){ email = s; }
void Bid::setQuantity(const int& i){ quantity = i; }

bool Bid::operator <(const Bid& b) const{
	return amount < b.amount;
}

bool Bid::operator ==(const Bid& b) const{
	return amount == b.amount;
}

istream &operator>>(istream &stream, Bid &b) {
	string s;
	float f;
	int i;
	Date d;

	getline(stream,s);
	b.setEmail(s);

	stream>>f;
	stream.get();
	b.setAmount(f);

	stream>>i;
	stream.get();
	b.setQuantity(i);

	stream>>d;
	stream.get();
	b.setDate(d);

	return stream;
}