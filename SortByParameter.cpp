#include "SortByParameter.h"

SortByParameter::SortByParameter(string criterion) : criterion(criterion) {}

bool SortByParameter::operator()(Advertisement *a1, Advertisement *a2) 
{
        if (criterion == "email") {
            return (a1->getEmail() < a2->getEmail());
        }

        if (criterion == "start") {
            return (a1->getStart() < a2->getStart());
        }

        if (criterion == "close") {
            return (a1->getClose() < a2->getClose());
        }

        if (criterion == "quantity") {
            return (a1->getQuantity() < a2->getQuantity());
        }

		if (criterion == "highest") 
		{
			if (!a1->getBids().size())
			{
				return false;
			}
			
			if (!a2->getBids().size())
			{
				return true;
			}
			return a2->getBids().top() < a1->getBids().top();
		}

		if (criterion == "lowest")
		{
			if (!a2->getBids().size()) 
			{
				return false;
			}

			if (!a1->getBids().size())
			{
				return true;
			}
			return a1->getBids().top() < a2->getBids().top();
		}
        return false;
}
